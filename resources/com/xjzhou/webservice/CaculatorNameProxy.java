package com.xjzhou.webservice;

public class CaculatorNameProxy implements com.xjzhou.webservice.CaculatorName {
  private String _endpoint = null;
  private com.xjzhou.webservice.CaculatorName caculatorName = null;
  
  public CaculatorNameProxy() {
    _initCaculatorNameProxy();
  }
  
  public CaculatorNameProxy(String endpoint) {
    _endpoint = endpoint;
    _initCaculatorNameProxy();
  }
  
  private void _initCaculatorNameProxy() {
    try {
      caculatorName = (new com.xjzhou.webservice.CaculatorServiceLocator()).getCaculatorPortName();
      if (caculatorName != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)caculatorName)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)caculatorName)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (caculatorName != null)
      ((javax.xml.rpc.Stub)caculatorName)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.xjzhou.webservice.CaculatorName getCaculatorName() {
    if (caculatorName == null)
      _initCaculatorNameProxy();
    return caculatorName;
  }
  
  public int xyz(int arg0, int arg1) throws java.rmi.RemoteException{
    if (caculatorName == null)
      _initCaculatorNameProxy();
    return caculatorName.xyz(arg0, arg1);
  }
  
  public void foo2() throws java.rmi.RemoteException{
    if (caculatorName == null)
      _initCaculatorNameProxy();
    caculatorName.foo2();
  }
  
  public void foo() throws java.rmi.RemoteException{
    if (caculatorName == null)
      _initCaculatorNameProxy();
    caculatorName.foo();
  }
  
  public int abc(int arg0, int arg1) throws java.rmi.RemoteException{
    if (caculatorName == null)
      _initCaculatorNameProxy();
    return caculatorName.abc(arg0, arg1);
  }
  
  
}