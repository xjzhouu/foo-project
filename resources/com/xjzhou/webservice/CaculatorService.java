/**
 * CaculatorService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.xjzhou.webservice;

public interface CaculatorService extends javax.xml.rpc.Service {
    public java.lang.String getCaculatorPortNameAddress();

    public com.xjzhou.webservice.CaculatorName getCaculatorPortName() throws javax.xml.rpc.ServiceException;

    public com.xjzhou.webservice.CaculatorName getCaculatorPortName(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
