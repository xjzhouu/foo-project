/**
 * CaculatorServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.xjzhou.webservice;

public class CaculatorServiceLocator extends org.apache.axis.client.Service implements com.xjzhou.webservice.CaculatorService {

    public CaculatorServiceLocator() {
    }


    public CaculatorServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CaculatorServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for CaculatorPortName
    private java.lang.String CaculatorPortName_address = "http://localhost:6000/Caculator";

    public java.lang.String getCaculatorPortNameAddress() {
        return CaculatorPortName_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CaculatorPortNameWSDDServiceName = "CaculatorPortName";

    public java.lang.String getCaculatorPortNameWSDDServiceName() {
        return CaculatorPortNameWSDDServiceName;
    }

    public void setCaculatorPortNameWSDDServiceName(java.lang.String name) {
        CaculatorPortNameWSDDServiceName = name;
    }

    public com.xjzhou.webservice.CaculatorName getCaculatorPortName() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CaculatorPortName_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCaculatorPortName(endpoint);
    }

    public com.xjzhou.webservice.CaculatorName getCaculatorPortName(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.xjzhou.webservice.CaculatorPortNameBindingStub _stub = new com.xjzhou.webservice.CaculatorPortNameBindingStub(portAddress, this);
            _stub.setPortName(getCaculatorPortNameWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCaculatorPortNameEndpointAddress(java.lang.String address) {
        CaculatorPortName_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.xjzhou.webservice.CaculatorName.class.isAssignableFrom(serviceEndpointInterface)) {
                com.xjzhou.webservice.CaculatorPortNameBindingStub _stub = new com.xjzhou.webservice.CaculatorPortNameBindingStub(new java.net.URL(CaculatorPortName_address), this);
                _stub.setPortName(getCaculatorPortNameWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CaculatorPortName".equals(inputPortName)) {
            return getCaculatorPortName();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webservice.xjzhou.com", "CaculatorService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webservice.xjzhou.com", "CaculatorPortName"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CaculatorPortName".equals(portName)) {
            setCaculatorPortNameEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
