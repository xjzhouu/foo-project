package com.xjzhou.jni;

/**
 * JNI : Java Native Interface
 * @author xjzhou
 *
 */
public class JNIDemo {
	
	public native String sayHi();
	
	static{
//		System.loadLibrary("JNIDemo");
		System.out.println(System.getProperties().get("java.library.path"));
		
		
	}
	
	
	public void print(){
		String result = sayHi();
		System.out.println(result);
	}
	
	
	public static void main(String[] args) {
//		new JNIDemo().print();
	}
}
