package com.xjzhou.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 互斥锁
 * @author xjzhou
 *
 */
public class LockDemo {

	private static Lock lock = new ReentrantLock();

	public static void main(String[] args) {

	}

	public static void lockTest() {
		lock.lock();
		try {

		} finally {
			lock.unlock();
		}
	}

	public static void tryLockTest() {
		if (lock.tryLock()) {
			try {

			} finally {
				lock.unlock();
			}
		} else {

		}
	}
}