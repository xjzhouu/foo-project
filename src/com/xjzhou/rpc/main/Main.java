package com.xjzhou.rpc.main;

import com.xjzhou.proxy.ProxyClient;
import com.xjzhou.service.api.FooService;
import com.xjzhou.service.api.impl.FooServiceImpl;

public class Main {

	public static void main(String[] args) {
/*		ProxyClient proxy = new ProxyClient(new FooServiceImpl());
		FooService obj = (FooService)proxy.getProxy();
		obj.printName("Sam");*/
		
		
		FooService obj = (FooService)ProxyClient.getProxy(new FooServiceImpl());
		obj.printName("Sam");
	}
}
