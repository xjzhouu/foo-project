package com.xjzhou.thread.safe;

import java.util.ArrayList;
import java.util.List;

public class ThreadDemo implements Runnable{
	
	private List<String> list = new ArrayList<String>();
	
	public synchronized void lock1(){
		System.out.println(Thread.currentThread().getName()+" arrive");
			synchronized(ThreadDemo.class){
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println(Thread.currentThread().getName()+" leave");
	}
	
	
	public synchronized void lock2(){
		
		System.out.println(Thread.currentThread().getName()+" arrive");
		
		synchronized(ThreadDemo.class){
			
		}
		System.out.println(Thread.currentThread().getName()+" leave");
	}
	
	public void foo3(){
		System.out.println(Thread.currentThread().getName()+" arrive");
		System.out.println(Thread.currentThread().getName()+" leave");
	}
	
	public void lockList1(){
		System.out.println(Thread.currentThread().getName()+" arrive ready to lock list");
		synchronized(list){
			System.out.println(Thread.currentThread().getName()+" lock list");
			try {
				Thread.sleep(10000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println(Thread.currentThread().getName()+" leave");
	}
	
	public void lockList2(){
		System.out.println(Thread.currentThread().getName()+" arrive");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(Thread.currentThread().getName()+" ready to add item for list");
		for(int i = 0;i<10;i++){
			System.out.println(" list add "+i);
			list.add("Foo - "+i);
		}
		System.out.println(Thread.currentThread().getName()+" leave");
	}

	public static void main(String[] args) {
		
		ThreadDemo thread0 = new ThreadDemo();
		
		Thread t1 = new Thread(new Runnable(){
			@Override
			public void run(){
				thread0.lock1();
			}
		} , "t1");
		
		Thread t2 = new Thread(new Runnable(){
			@Override
			public void run(){
				thread0.lock2();
			}
		} , "t2");
		
		t1.start();
		
		t2.start();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	@Override
	public void run() {
		// TODO Auto-generated method stub
	}
}
