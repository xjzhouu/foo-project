package com.xjzhou.webserivce;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class Client {

	public static void main(String[] args) {
		try {
			URL url = new URL("http://staging.lc-pcs-core.fund.xiaomi.srv/services/productionService?wsdl");
			
			// Qualified name of the service:
			//   1st arg is the service URI
			//   2nd is the service name published in the WSDL
			
			QName qname = new QName("http://service.core.pcs.lc.payment.com/", "ProductionServiceImplService");
			
			// Create, in effect, a factory for the service.
			
			Service service = Service.create(url, qname);
			
			// Extract the endpoint interface, the service "port".
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
