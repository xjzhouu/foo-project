package com.xjzhou.ws;

import javax.xml.rpc.ServiceException;
import javax.xml.ws.WebServiceRef;

import com.xjzhou.webservice.CaculatorName;
import com.xjzhou.webservice.CaculatorService;
import com.xjzhou.webservice.CaculatorServiceLocator;

public class CaculatorClient {
	
	@WebServiceRef(wsdlLocation="http://localhost:6000/Caculator?wsdl")
	private static CaculatorService service;
	
	public static void main(String[] args) {
		test2();
	}
	
	
	public static void test(){
		try {
			CaculatorName port = service.getCaculatorPortName();
			System.out.println(port.abc(1, 2));
			System.out.println(port.xyz(2, 3));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void test2(){
		 try {
			CaculatorName test = new CaculatorServiceLocator().getCaculatorPortName();
			System.out.println(test.abc(1, 2));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     
	}
}
