package com.xjzhou.ws;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService(name = "CaculatorName", serviceName = "CaculatorService", targetNamespace = "http://webservice.xjzhou.com", portName = "CaculatorPortName")
public class CaculatorImpl implements ICaculator {

	@WebMethod(operationName = "abc", action = "mulAction")
	@Override
	public int mul(int a, int b) {
		return a * b;
	}

	@WebMethod(operationName = "xyz", action = "addAction")
	@Override
	public int add(int a,int b) {
		return a + b;
	}

	@WebMethod
	@PostConstruct
	public void foo() {
		System.out.println("the implementing class begins responding to web service clients.");
	}

	@WebMethod
	@PreDestroy
	public void foo2() {
		System.out.println("the container before the endpoint is removed from operation");
	}
}
