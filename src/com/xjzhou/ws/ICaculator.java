package com.xjzhou.ws;

public interface ICaculator {
	
	int mul(int a, int b);
	
	int add(int a, int b);
}
