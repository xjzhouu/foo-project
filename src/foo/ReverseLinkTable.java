package foo;

public class ReverseLinkTable {

	public static void main(String[] args) {
		Node head = formLink(new Node(0));

		// 反转之前
		iteratorLink(head);

		// 反转
		Node newHead = reverseLinkTable3(head);

		// 反转之后
		iteratorLink(newHead);
		
		System.out.println(foo(5));

	}

	public static void iteratorLink(Node head) {
		while (head != null) {
			System.out.print(head.getData() + " ");
			head = head.getNext();
		}
		System.out.println();
	}

	public static Node formLink(Node head) {
		Node temp = head;
		for (int i = 0; i < 5; i++) {
			head.setNext(new Node(i + 1));
			head = head.getNext();
		}

		// iteratorLink(temp);
		return temp;
	}

	public static Node reverseLinkTable(Node head) {
		// 链表为空链表 或者是 到达链表尾结点
		if (head == null || head.getNext() == null) {
			return head;
		}

		// 将尾结点作为新的节点
		Node cur = reverseLinkTable(head.getNext());
		head.getNext().setNext(head);
		head.setNext(null);
		return cur;
	}

	public static Node reverseLinkTable2(Node head) {

		if (head == null) {
			return head;
		}
		Node pre = head;// 上一结点
		Node cur = head.getNext();// 当前结点
		Node tmp;// 临时结点，用于保存当前结点的指针域（即下一结点）
		while (cur != null) {// 当前结点为null，说明位于尾结点
			tmp = cur.getNext();
			cur.setNext(pre);// 反转指针域的指向

			// 指针往下移动
			pre = cur;
			cur = tmp;
		}
		// 最后将原链表的头节点的指针域置为null，还回新链表的头结点，即原链表的尾结点
		head.setNext(null);

		return pre;

	}

	public static Node reverseLinkTable3(Node head) {
		if (head == null) {
			return head;
		}
		Node tmp = head;
		Node cur = head.getNext();
		while (cur != null) {
			Node newNode = cur.getNext();
			cur.setNext(tmp);
			tmp = cur;
			cur = newNode;
		}
		head.setNext(null);
		return tmp;
	}
	
	public static int foo(int n){
		if(n == 1){
			return n;
		}
		int tmp = foo(n-1);
		
		return tmp;
	}
}

class Node {

	Node(int data) {
		this.data = data;
	}

	private int data;

	private Node next;

	public int getData() {
		return data;
	}

	public void setData(int data) {
		this.data = data;
	}

	public Node getNext() {
		return next;
	}

	public void setNext(Node next) {
		this.next = next;
	}

}
