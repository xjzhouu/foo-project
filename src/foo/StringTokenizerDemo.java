package foo;

import java.util.StringTokenizer;

public class StringTokenizerDemo {
	
	public static void main(String[] args) {
		StringTokenizer token = new StringTokenizer("2,3,4,5.6,7.8.9,2\n,/r,/t/f,10,3,0,-1",",./r/t/f",true);
		
		System.out.println(token.countTokens());
		
		while(token.hasMoreElements()){
			System.out.println(token.nextElement());
		}
	}
}
