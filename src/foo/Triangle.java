package foo;

public class Triangle {

	public static void main(String[] args) throws CustomException {
		try {
			int len = Integer.valueOf(args[0]);
			System.out.println("*");
			for (int i = 1; i < len; i++) {
				String temp = "";
				int k = i * 2 - 1;
				for (int j = 0; j < k; j++) {
					if(i == len -1){
						temp = printX(k);
						break;
					}
					temp += i;
				}
				System.out.println("*" + temp +"*");
			}
			
		} catch (Exception e) {
			throw new CustomException("非法输入");
		}
	}

	public static String printX(int count) {
		
		StringBuffer s = new StringBuffer();

		for (int i = 0; i < count; i++) {
			s.append("*");
		}

		return s.toString();
	}
}

class CustomException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	CustomException(String msg) {
		super(msg);
	}
}
